<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_cms extends CI_Model {
	
	public function get_data_food(){
		$query = "
			SELECT *
			FROM food_desc
			ORDER BY id DESC
		";
		return $this->db->query($query)->result();
	}
	
	public function by_id($id){
		$datasrc = $this->db->get_where('food_desc', array('id' => $id));
		return $datasrc->num_rows() > 0 ? $datasrc->row() : $this;
	}	
}