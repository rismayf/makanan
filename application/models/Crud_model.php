<?php
class Crud_model extends CI_Model 
{
	
	public function Insert($table,$data){
        $res = $this->db->insert($table, $data); 
		return $res;
    }

	public function Update($table, $data, $where){
        $res = $this->db->update($table, $data, $where); 
        return $res;
    }
 
    public function Delete($table, $where){
        $res = $this->db->delete($table, $where); 
        return $res;
    }

	public function GetWhere($table, $data){
		$res=$this->db->get_where($table, $data);
		return $res->result_array();
	}
}