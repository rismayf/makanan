<?php


class User_model extends CI_Model
{
	private $_table = "users";

	public function isNotLogin(){
		return $this->session->userdata('user_logged') === null;
	}

	function checkLogin($table,$where){
		return $this->db->get_where($table,$where);
	}

}
