<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Pengenalan Masakan Indonesia</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="<?php echo base_url('/themes/assets/favicon.ico'); ?>" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        
        <link href="<?php echo base_url('/themes/css/styles.css'); ?>" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="<?php echo site_url('home') ?>">
                    <span><i class="fa fa-angle-left"></i> Halaman Utama</span>
                </a>
            </div>
        </nav>
        <section class="page-section bg-light" id="portfolio">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Masakan Indonesia</h2>
                    <div class="row  d-flex justify-content-end">
                        <div class="col-lg-4 d-flex justify-content-end" style="padding: 20px">
                            <h4 style="margin-top: 8px; margin-right: 10px;">
                                <i class="fas fa-search text-secondary"></i>
                            </h4>  
                            <input class="form-control" id="search" type="text" placeholder="Cari Makanan" />                          
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php 
                        $id = 1;
                        foreach($grid as $record) :
                    ?>
                        <div class="col-lg-4 col-sm-6 mb-4">
                            <div class="portfolio-item">
                                <a class="portfolio-link" data-bs-toggle="modal" href="#modalMakanan<?php echo $id; ?>">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                    </div>
                                    <img class="img-fluid" src="<?=base_url().'uploads/'.$record->picture;?>">
                                </a>
                                <div class="portfolio-caption">
                                    <div class="portfolio-caption-heading"><?php echo $record->name; ?></div>
                                    <div class="portfolio-caption-subheading text-muted"><?php echo $record->city_from; ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-modal modal fade" id="modalMakanan<?php echo $id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="close-modal" data-bs-dismiss="modal"><img src="<?php echo base_url('/themes/assets/img/close-icon.svg'); ?>"  alt="Close modal" /></div>
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-lg-8">
                                                <div class="modal-body">
                                                    <!-- Project details-->
                                                    <h2 class="text-uppercase"><?php echo $record->name; ?></h2>
                                                    <p class="item-intro text-muted"><?php echo $record->city_from; ?></p>
                                                    <?php
                                                        echo '<img class="img-fluid d-block mx-auto" src="data:image/jpeg;base64,'.base64_encode( $record->picture ).'"/>';
                                                    ?>
                                                    <p>
                                                        <?php echo $record->description; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php 
                        $id++;
                        endforeach; 
                    ?>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-start">Copyright &copy; Tim 2 Pemrograman Web 2021</div>
                </div>
            </div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="<?php echo base_url('/themes/js/scripts.js'); ?>"></script>
        <!-- <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script> -->
    </body>
</html>
