<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Pengenalan Masakan Indonesia</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="<?php echo base_url('/themes/assets/favicon.ico'); ?>" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        
        <link href="<?php echo base_url('/themes/css/styles.css'); ?>" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="#page-top">
                    <!-- <img src="<?php echo base_url('/themes/assets/img/navbar-logo.svg'); ?>" alt="..." /> -->
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ms-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                        <li class="nav-item"><a class="nav-link" href="#services">Tentang</a></li>
                        <li class="nav-item"><a class="nav-link" href="#portfolio">Masakan</a></li>
                        <li class="nav-item"><a class="nav-link" href="#contact">Kontak</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <div class="masthead-subheading">Website Pengenalan</div>
                <div class="masthead-heading text-uppercase">Masakan Indonesia</div>
                <a class="btn btn-primary btn-xl text-uppercase" href="#services">Pelajari lebih lanjut</a>
            </div>
        </header>
        <!-- Services-->
        <section class="page-section" id="services">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Tentang Website</h2>
                    <h3 class="section-subheading text-muted">Website ini mengajak kamu untuk mengenal makanan khas dari berbagai penjuru Indonesia</h3>
                </div>
                <div class="row text-center">
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fas fa-map-marker-alt fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="my-3">Asal Daerah</h4>
                        <p class="text-muted">
                            Anda dapat mengetahui asal daerah dari makanan yang anda lihat
                        </p>
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fas fa-copy fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="my-3">Deskripsi</h4>
                        <p class="text-muted">
                            Dilengkapi dengan deskripsi singkat mengenai makanan
                        </p>
                    </div>
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fas fa-image fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="my-3">Gambar</h4>
                        <p class="text-muted">
                            Anda dapat mengetahui bentuk sajian dari masakan dengan melihat gambar
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- Portfolio Grid-->
        <section class="page-section bg-light" id="portfolio">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Masakan terpopuler</h2>
                    <a href="<?php echo site_url('makanan') ?>">
                        <h3 class="section-subheading text-muted">Lihat Lebih lanjut ----></h3>
                    </a>
                </div>
                <div class="row">
                    <?php 
                        $id = 1;
                        foreach($grid as $record) :
                    ?>
                        <div class="col-lg-4 col-sm-6 mb-4">
                            <div class="portfolio-item">
                                <a class="portfolio-link" data-bs-toggle="modal" href="#modalMakanan<?php echo $id; ?>">
                                    <div class="portfolio-hover">
                                        <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                    </div>
                                    <img class="img-fluid" src="<?=base_url().'uploads/'.$record->picture;?>">
                                </a>
                                <div class="portfolio-caption">
                                    <div class="portfolio-caption-heading"><?php echo $record->name; ?></div>
                                    <div class="portfolio-caption-subheading text-muted"><?php echo $record->city_from; ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-modal modal fade" id="modalMakanan<?php echo $id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="close-modal" data-bs-dismiss="modal"><img src="<?php echo base_url('/themes/assets/img/close-icon.svg'); ?>"  alt="Close modal" /></div>
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-lg-8">
                                                <div class="modal-body">
                                                    <!-- Project details-->
                                                    <h2 class="text-uppercase"><?php echo $record->name; ?></h2>
                                                    <p class="item-intro text-muted"><?php echo $record->city_from; ?></p>
                                                    <?php
                                                        echo '<img class="img-fluid d-block mx-auto" src="data:image/jpeg;base64,'.base64_encode( $record->picture ).'"/>';
                                                    ?>
                                                    <p>
                                                        <?php echo $record->description; ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php 
                        $id++;
                        endforeach; 
                    ?>
                </div>
            </div>
        </section>
        <!-- Contact-->
        <section class="page-section" id="contact">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Kontak Kami</h2>
                    <h3 class="section-subheading text-muted">Untuk kritik dan saran harap isi form berikut:</h3>
                </div>
                <form id="contactForm" data-sb-form-api-token="API_TOKEN">
                    <div class="row align-items-stretch mb-5">
                        <div class="col-md-6">
                            <div class="form-group">
                                <!-- Name input-->
                                <input class="form-control" id="name" type="text" placeholder="Nama Anda *" data-sb-validations="required" />
                                <div class="invalid-feedback" data-sb-feedback="name:required">Nama wajib diisi.</div>
                            </div>
                            <div class="form-group">
                                <!-- Email address input-->
                                <input class="form-control" id="email" type="email" placeholder="Email *" data-sb-validations="required,email" />
                                <div class="invalid-feedback" data-sb-feedback="email:required">Email wajib diisi.</div>
                                <div class="invalid-feedback" data-sb-feedback="email:email">Email tidak valid.</div>
                            </div>
                            <div class="form-group mb-md-0">
                                <!-- Phone number input-->
                                <input class="form-control" id="phone" type="tel" placeholder="Nomor HP *" data-sb-validations="required" />
                                <div class="invalid-feedback" data-sb-feedback="phone:required">Nomor HP wajib diisi.</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-group-textarea mb-md-0">
                                <!-- Message input-->
                                <textarea class="form-control" id="message" placeholder="Pesan *" data-sb-validations="required"></textarea>
                                <div class="invalid-feedback" data-sb-feedback="message:required">Pesan wajib diisi.</div>
                            </div>
                        </div>
                    </div>
                    <div class="d-none" id="submitSuccessMessage">
                        <div class="text-center text-white mb-3">
                            <div class="fw-bolder">Berhasil mengirim pesan</div>
                        </div>
                    </div>
                    <!-- Submit error message-->
                    <!---->
                    <!-- This is what your users will see when there is-->
                    <!-- an error submitting the form-->
                    <div class="d-none" id="submitErrorMessage"><div class="text-center text-danger mb-3">Error dalam mengirim pesan!</div></div>
                    <!-- Submit Button-->
                    <div class="text-center"><button class="btn btn-primary btn-xl text-uppercase disabled" id="submitButton" type="submit">Kirim Pesan</button></div>
                </form>
            </div>
        </section>
        <!-- Footer-->
        <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-start">Copyright &copy; Tim 2 Pemrograman Web 2021</div>
                </div>
            </div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="<?php echo base_url('/themes/js/scripts.js'); ?>"></script>
        <!-- <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script> -->
    </body>
</html>
