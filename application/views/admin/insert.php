<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Pengenalan Masakan Indonesia</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="<?php echo base_url('/themes/assets/favicon.ico'); ?>" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        
        <link href="<?php echo base_url('/themes/css/styles.css'); ?>" rel="stylesheet" />
    </head>
    <body>
        <div class="row">
            <div class="col-lg-12 bg-dark">
                <div class="container pt-3 pb-3">
                    <a class="navbar-brand" href="<?php echo site_url('cms'); ?>">
                        <span>
                            <i class="fa fa-angle-left"></i> Kembali</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 p-5 border">
                    <div class="container">
                        <div class="row d-flex justify-content-center">
                            <div class="col-lg-5 col-md-8 col-sm-12">
								<?php
								echo form_open_multipart(base_url().'Crud/insert'); ?>
                                    <h4>Tambah Data Makanan Baru</h4>
                                    <hr>
                                    <div class="form-group">
                                        <label class="mb-1" for="name">Nama</label>
                                        <input type="text" class="form-control" id="name" name="name" value="">
                                    </div>
                                    <div class="form-group mt-2">
                                        <label class="mb-1" for="city_from">Asal Daerah</label>
                                        <input type="text" class="form-control" id="city_from" name="city_from" value="">
                                    </div>
                                    <div class="form-group mt-2">
                                        <label class="mb-1" for="description">Deskripsi</label>
                                        <textarea class="form-control" name="description" id="description" rows="3"></textarea>
                                    </div>
                                    <div class="form-group mt-2">
                                        <label class="mb-1" for="picture">Gambar (.jpg / .png)</label>
                                        <input type="file" class="form-control" id="image" name="image">
                                    </div>
                                    <div class="form-group mt-2">
                                        <label class="mb-1" for="type">Tipe Makanan</label>
                                        <select class="form-control" name="type" id="type">
                                            <option value="0" selected disabled>Pilih Tipe Makanan</option>
                                            <option value="1">Hidangan Pembuka</option>
                                            <option value="2">Hidangan Utama</option>
                                            <option value="3">Hidangan Penutup / Camilan</option>
                                            <option value="4">Minuman</option>
                                        </select>
                                    </div>
                                    <div class="form-group mt-3">
                                        <button type="submit" name="save" class="btn btn-primary">Simpan</button>
                                    </div>
								<?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <!-- <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-start">Copyright &copy; Tim 2 Pemrograman Web 2021</div>
                </div>
            </div>
        </footer> -->
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="<?php echo base_url('/themes/js/scripts.js'); ?>"></script>
        <!-- <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script> -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    </body>
</html>
