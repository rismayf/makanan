<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Pengenalan Masakan Indonesia</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="<?php echo base_url('/themes/assets/favicon.ico'); ?>" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        
        <link href="<?php echo base_url('/themes/css/styles.css'); ?>" rel="stylesheet" />
    </head>
    <body>
        <div class="row">
            <div class="col-lg-12 bg-dark">
                <div class="container pt-3 pb-3">
                    <h3 class="text-white">Content Management System</h3>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 p-5 border">
                    <div class="container">
                    <a class="btn btn-primary mb-3" href="<?php echo site_url('crud') ?>">
                        <i class="fa fa-plus"></i> Tambah Makanan Baru
                    </a>
					<a class="btn btn-danger mb-3" style="float: right;" href="<?php echo base_url().'Login/logout' ?>">
                        <i class="fa fa-lock"></i> Keluar
                    </a>
                    <table class="table table-responsive table-bordered table-hover" id="mydatatables">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="15%">Gambar</th>
                                <th width="15%">Nama</th>
                                <th width="10%">Asal Daerah</th>
                                <th width="50%">Deskripsi</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
								$no = 1;
								foreach($grid as $record) :
							?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><img width="100px" src="<?php echo base_url('uploads/'.$record->picture) ?>" /></td>
                                <td><?php echo $record->name; ?></td>
                                <td><?php echo $record->city_from; ?></td>
                                <td><?php echo $record->description; ?></td>
                                <td>
                                    <div style="display: flex; gap: 10px">
                                        <a class="btn btn-warning" href="<?php echo base_url()."Crud/edit/".$record->id; ?>" data-toggle="modal"><i class="fa fa-edit" data-toggle="tooltip" title="Edit"></i></a>
                                        <a class="btn btn-danger" href="<?php echo base_url()."Crud/delete/".$record->id; ?>" data-toggle="modal"><i class="fa fa-trash" data-toggle="tooltip" title="Delete"></i></a>
                                    </div>
                                </td>
                            </tr>
                             <?php
							 	$no++;
							  endforeach;
							 ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <!-- <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-start">Copyright &copy; Tim 2 Pemrograman Web 2021</div>
                </div>
            </div>
        </footer> -->
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="<?php echo base_url('/themes/js/scripts.js'); ?>"></script>
        <!-- <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script> -->
    </body>
</html>
