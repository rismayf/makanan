<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
		$this->load->model('model_cms');	
	}

	public function index()
	{
		$this->load->view('admin/cms', array(
			'grid' => $this->model_cms->get_data_food(),
		));
	}
}
