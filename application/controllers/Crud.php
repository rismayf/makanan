<?php 
class Crud extends CI_Controller 
{
	public function __construct()
	{
		/*call CodeIgniter's default Constructor*/
		parent::__construct();
		/*load database libray manually*/
		$this->load->database();
		
		/*load Model*/
		$this->load->model('Crud_model');
	}

	public function index() {
		$this->load->view('admin/insert');
	}

	public function insert(){
		$this->load->model('Crud_model');

		$this->load->library('upload');
		$id = uniqid();
		$config['upload_path']          = 'uploads/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['file_name']            = $id;
		$config['overwrite']			= true;

		$this->upload->initialize($config);
		$image = 'dafault.jpg';
		if (!$this->upload->do_upload('image')) {
			print_r($this->upload->display_errors());
		} else {
			$image = $this->upload->data();
		}
		$data = array(
			'name' => $this->input->post('name'),
			'city_from' => $this->input->post('city_from'),
			'description' => $this->input->post('description'),
			'picture' => $image['file_name'],
			'type' => $this->input->post('type')
			 );
		$data = $this->Crud_model->Insert('food_desc', $data);
		redirect('cms','refresh');
	}
	
	public function delete($id) {
		$id = array('id' => $id);
		$this->load->model('Crud_model');
		$this->Crud_model->Delete('food_desc', $id);
		redirect('cms','refresh');
	}

	public function edit($id){
		$this->load->model('Crud_model');
		$getData = $this->Crud_model->GetWhere('food_desc', array('id' => $id));
		$data = array(
			'id' => $getData[0]['id'],
			'name' => $getData[0]['name'],
			'city_from' => $getData[0]['city_from'],
			'description' => $getData[0]['description'],
			'picture' => $getData[0]['picture'],
			'type' => $getData[0]['type'],
			);
		$this->load->view('admin/edit', $data); 
	}

	public function update($id){
		$name = $_POST['name'];
		$city_from = $_POST['city_from'];
		$description = $_POST['description'];
		// $picture = $_POST['picture'];
		$type = $_POST['type'];
		$data = array(
			'name' => $name,
			'city_from' => $city_from,
			'description' => $description,
			// 'picture' => $picture,
			'type' => $type,
		 );
		$where = array(
			'id' => $id,
		);
		$this->load->model('Crud_model');
		$res = $this->Crud_model->Update('food_desc', $data, $where);
		if ($res>0) {
			redirect('cms','refresh');
		}
	}
}
?>
