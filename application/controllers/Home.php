<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('model_cms');	
	}

	public function index()
	{
		$this->load->view('main/home', array(
			'grid' => $this->model_cms->get_data_food()
		));
	}
}
